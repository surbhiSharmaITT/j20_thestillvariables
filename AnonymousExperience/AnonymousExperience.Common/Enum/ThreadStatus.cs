﻿namespace AnonymousExperience.Common.Enum
{
    public enum ThreadStatus
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3
    }
}
