﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonymousExperience.Common.Enum
{
    public enum Roles
    {
        Public = 1,
        Admin = 2
    }
}
