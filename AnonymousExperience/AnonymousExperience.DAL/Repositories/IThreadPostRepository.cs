﻿using AnonymousExperience.DAL.Generic;
using AnonymousExperience.Model;

namespace AnonymousExperience.DAL.Repositories
{
    public interface IThreadPostRepository : IGenericRepository<ThreadPost>
    {
        void AddLikesToPost(int userId, int postId);
        int GetTotalLikes(int postId);
    }
}
