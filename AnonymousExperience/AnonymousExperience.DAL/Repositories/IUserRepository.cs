﻿using AnonymousExperience.DAL.Generic;
using AnonymousExperience.Model;

namespace AnonymousExperience.DAL.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {

    }
}
