﻿using AnonymousExperience.DAL.Generic;
using AnonymousExperience.Model;

namespace AnonymousExperience.DAL.Repositories
{
    public class UserRepository : GenericRepository<AnonymousExperienceEntities, User>, IUserRepository
    {

    }
}
