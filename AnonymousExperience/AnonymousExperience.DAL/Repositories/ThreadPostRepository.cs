﻿using System.Linq;
using AnonymousExperience.DAL.Generic;
using AnonymousExperience.Model;

namespace AnonymousExperience.DAL.Repositories
{
    public class ThreadPostRepository : GenericRepository<AnonymousExperienceEntities, ThreadPost>, IThreadPostRepository
    {
        public void AddLikesToPost(int userId, int postId)
        {
            Context.PostLikes.Add(new PostLike {UserId = userId, PostId = postId});
            Context.SaveChanges();
        }

        public int GetTotalLikes(int postId)
        {
            return Context.PostLikes.Count(x => x.PostId == postId);
        }
    }
}
