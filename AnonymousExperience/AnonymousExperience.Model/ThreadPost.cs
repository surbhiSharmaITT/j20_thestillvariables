//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AnonymousExperience.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ThreadPost
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ThreadPost()
        {
            this.PostLikes = new HashSet<PostLike>();
            this.ThreadPost1 = new HashSet<ThreadPost>();
            this.ThreadPostTags = new HashSet<ThreadPostTag>();
        }
    
        public int Id { get; set; }
        public string Content { get; set; }
        public int CategoryId { get; set; }
        public int StatusId { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public Nullable<System.DateTime> StatusChangedAt { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> StatusChangedBy { get; set; }
        public bool Active { get; set; }
        public Nullable<int> ParentId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PostLike> PostLikes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThreadPost> ThreadPost1 { get; set; }
        public virtual ThreadPost ThreadPost2 { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThreadPostTag> ThreadPostTags { get; set; }
    }
}
