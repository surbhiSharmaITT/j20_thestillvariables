﻿
CREATE DATABASE AnonymousExperience
GO

USE AnonymousExperience
GO
/****** Object:  Table [dbo].[Users]    Script Date: 07/02/2020 11:49:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[PasswordHash] [varchar](100) NOT NULL,
	[SecurityStamp] [varchar](250) NULL,
	[IsActive] [bit] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[CreateDate] [datetime2](7) NULL,
	[LastPasswordChangeDate] [datetime2](7) NULL,
	[LockoutEndDate] [datetime2](7) NULL,
	[AccessFailedCount] [int] NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[UserName] [varchar](100) NOT NULL,
	[PasswordToken] [varchar](max) NULL,
	CONSTRAINT [PK_User] PRIMARY KEY ([Id])
)
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[Role]    Script Date: 07/02/2020 12:36:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


/****** Object:  Table [dbo].[UserRole]    Script Date: 07/02/2020 11:49:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserRole](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRole_dbo.Role_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_dbo.UserRole_dbo.Role_RoleId]
GO

ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRole_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_dbo.UserRole_dbo.Users_UserId]
GO

/****** Object:  Table [dbo].[UserLogin]    Script Date: 07/02/2020 11:49:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserLogin](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserLogin]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserLogin_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserLogin] CHECK CONSTRAINT [FK_dbo.UserLogin_dbo.Users_UserId]
GO


/****** Object:  Table [dbo].[UserClaim]    Script Date: 07/02/2020 11:50:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserClaim](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_UserClaim] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserClaim]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserClaim_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserClaim] CHECK CONSTRAINT [FK_dbo.UserClaim_dbo.Users_UserId]
GO


CREATE TABLE [dbo].[ThreadPost]
(
	[Id] INT IDENTITY(1,1),
	[Content] [nvarchar](MAX) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[CreatedAt] DATETIME2 NOT NULL,
	[StatusChangedAt] DATETIME2 NULL, 
    [CreatedBy] INT NULL, 
    [StatusChangedBy] INT NULL, 
    [Active] BIT NOT NULL,
	[ParentId] INT NULL,
    CONSTRAINT [PK_ThreadPost] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_ThreadPost_UserCreated] FOREIGN KEY ([CreatedBy]) REFERENCES [User]([Id]),
	CONSTRAINT [FK_ThreadPost_UserStatusChanged] FOREIGN KEY ([StatusChangedBy]) REFERENCES [User]([Id]),
	CONSTRAINT [FK_ThreadPost_ParentThreadPost] FOREIGN KEY ([ParentId]) REFERENCES [ThreadPost]([Id])
)
GO

CREATE TABLE [dbo].[ThreadPostTag]
(
	[Id] INT IDENTITY(1,1),
	[UserId] INT NOT NULL, 
	[ThreadPostId] INT NOT NULL,
    CONSTRAINT [PK_ThreadPostTag] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_ThreadPostTag_User] FOREIGN KEY ([UserId]) REFERENCES [User]([Id]),
	CONSTRAINT [FK_ThreadPostTag_ThreadPost] FOREIGN KEY ([ThreadPostId]) REFERENCES [ThreadPost]([Id])
)


/****** Object:  Table [dbo].[PostLike]    Script Date: 7/2/2020 2:00:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PostLike](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PostId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_PostLike] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PostLike]  WITH CHECK ADD  CONSTRAINT [FK_PostLikes_ThreadPost] FOREIGN KEY([PostId])
REFERENCES [dbo].[ThreadPost] ([Id])
GO

ALTER TABLE [dbo].[PostLike] CHECK CONSTRAINT [FK_PostLikes_ThreadPost]
GO

ALTER TABLE [dbo].[PostLike]  WITH CHECK ADD  CONSTRAINT [FK_PostLikes_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[PostLike] CHECK CONSTRAINT [FK_PostLikes_Users]
GO


INSERT INTO Role 
VALUES('Admin', 'Admin')
GO

INSERT INTO Role 
VALUES('Public', 'Public')
GO