﻿using System;
using System.Collections.Generic;
using AnonymousExperience.Common.Enum;

namespace AnonymousExperience.API.ViewModels
{
    public class ThreadPostModel
    {
        public string Content { get; set; }
        public int CategoryId { get; set; }
        public int? CreatedBy { get; set; }
        public int? ParentId { get; set; }
        public List<int> Tags { get; set; }
    }

    public class PostApprovalRequest
    {
        public int ThreadPostId { get; set; }
        public ThreadStatus StatusId { get; set; }
        public int ApprovedBy { get; set; }
    }

    public class ThreadPostViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int CategoryId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public int TotalLikes { get; set; }
        public IEnumerable<TagViewModel> Tags { get; set; }
        public IEnumerable<CommentViewModel> ChildThreads { get; set; }
    }

    public class TagViewModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
    }

    public class CommentViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int CategoryId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public ThreadStatus StatusId { get; set; }
        public IEnumerable<CommentTagViewModel> Tags { get; set; }
    }

    public class CommentTagViewModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
    }

    public class LikePostModel
    {
        public int PostId { get; set; }
        public int UserId { get; set; }
    }

}