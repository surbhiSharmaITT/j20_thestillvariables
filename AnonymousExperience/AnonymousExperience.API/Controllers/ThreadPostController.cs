﻿using System.Collections.Generic;
using System.Web.Http;
using AnonymousExperience.API.BusinessLogic;
using AnonymousExperience.API.ViewModels;

namespace AnonymousExperience.API.Controllers
{
    [RoutePrefix("Post")]
    public class ThreadPostController : ApiController
    {
        private readonly IThreadPostManager _postManager;

        public ThreadPostController(IThreadPostManager postManager)
        {
            _postManager = postManager;
        }

        [Route("GetApproved")]
        [HttpGet]
        public List<ThreadPostViewModel> GetApprovedPosts()
        {
            return _postManager.GetAllApprovedPosts();
        }

        [Route("Create")]
        [HttpPost]
        public IHttpActionResult CreatePostOrReply(ThreadPostModel post)
        {
            _postManager.CreatePost(post);
            return Ok();
        }

        [Route("Approve")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IHttpActionResult ApprovePost(PostApprovalRequest postApproval)
        {
            _postManager.PostApproval(postApproval);
            return Ok();
        }

        [Route("Approve")]
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public List<ThreadPostViewModel> GetPostsToApprove()
        {
           return _postManager.GetPostsForApproval();
        }

        [Route("Like")]
        [HttpPost]
        [Authorize]
        public IHttpActionResult LikePost(LikePostModel post)
        {
            _postManager.LikePost(post);
            return Ok();
        }
    }
}
