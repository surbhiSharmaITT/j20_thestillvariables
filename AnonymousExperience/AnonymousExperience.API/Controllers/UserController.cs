﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using AnonymousExperience.API.BusinessLogic;
using System.Web.Http;
using System.Web.Routing;
using AnonymousExperience.API.Models;
using AnonymousExperience.API.ViewModels;
using AnonymousExperience.Common.Enum;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace AnonymousExperience.API.Controllers
{
    [RoutePrefix("User")]
    public class UserController : ApiController
    {
        private IUserManagement _userManagement;
        private ApplicationUserManager _userManager;
        
        public UserController(IUserManagement userManagement)
        {
            _userManagement = userManagement;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        [Route("GetAllUser")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult GetAllUser()
        {
            var users = _userManagement.GetAllUser();
            if (users == null)
            {
                // Returns a NotFoundResult
                return NotFound();
            }
            // Returns an OkNegotiatedContentResult
            return Ok(users);
        }

        [Route("GetUserById")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult GetUserByUserId(int userId)
        {
            var user = _userManagement.GetUserByUserId(userId);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        [Route("GetUserByUserName")]
        [HttpGet]
        public IHttpActionResult GetUserByUserName(string userName)
        {
            var user = _userManagement.GetUserByUserName(userName);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        [HttpPost]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
                
            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                FullName = model.FullName,
                IsActive = true,
                IsLocked = false,
                CreateDate = DateTime.UtcNow
            };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return Ok(result);
            }
            await UserManager.AddToRoleAsync(user.Id,
                    model.Role == "Admin" ? Roles.Admin.ToString() : Roles.Public.ToString());
            return Ok(result);
        }
    }
}