﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using AnonymousExperience.API.BusinessLogic;
using AnonymousExperience.DAL.Generic;
using AnonymousExperience.DAL.Repositories;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;


namespace AnonymousExperience.API
{
    public class IocConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType(typeof(Model.AnonymousExperienceEntities)).AsSelf().InstancePerLifetimeScope();

            //Registering Generic Repository
            builder.RegisterGeneric(typeof(GenericRepository<,>)).As(typeof(IGenericRepository<>)).InstancePerRequest();

            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerRequest();
            builder.RegisterType<ThreadPostRepository>().As<IThreadPostRepository>().InstancePerRequest();

            builder.RegisterType<UserManagement>().As<IUserManagement>().InstancePerRequest();
            builder.RegisterType<ThreadPostManager>().As<IThreadPostManager>().InstancePerRequest();
            
            builder.RegisterControllers(Assembly.GetExecutingAssembly()); //Register MVC Controllers
            builder.RegisterSource(new ViewRegistrationSource());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()); //Register WebApi Controllers
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container)); //Set the MVC DependencyResolver
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver((IContainer)container); //Set the WebApi DependencyResolver
        }
    }
}