﻿using System.Collections.Generic;
using AnonymousExperience.Model;
using System.Data.Entity.Infrastructure;
using System.Linq;
using AnonymousExperience.API.ViewModels;

namespace AnonymousExperience.API.BusinessLogic
{
    public interface IUserManagement
    {
        List<UserModel> GetAllUser();
        UserModel GetUserByUserId(int userId);
        UserModel GetUserByUserName(string userName);
    }
}
