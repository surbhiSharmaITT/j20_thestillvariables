﻿using System;
using System.Collections.Generic;
using System.Linq;
using AnonymousExperience.API.ViewModels;
using AnonymousExperience.Common.Enum;
using AnonymousExperience.DAL.Repositories;
using AnonymousExperience.Model;
using AutoMapper;

namespace AnonymousExperience.API.BusinessLogic
{
    public class ThreadPostManager : IThreadPostManager
    {
        private readonly IThreadPostRepository _postRepository;

        public ThreadPostManager(IThreadPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public void CreatePost(ThreadPostModel threadPost)
        {
            var post = Mapper.Map<ThreadPostModel, ThreadPost>(threadPost);
            post.CreatedAt = DateTime.Now;
            post.Active = true;
            post.StatusId = threadPost.CreatedBy != null ? (int)ThreadStatus.Approved : (int)ThreadStatus.Pending;
            post.ThreadPostTags = threadPost.Tags.Select(x => new ThreadPostTag { UserId = x }).ToList();
            _postRepository.Add(post);
            _postRepository.Save();
        }

        public void PostApproval(PostApprovalRequest postApproval)
        {
            var post = _postRepository.FindBy(x => x.Id == postApproval.ThreadPostId).First();
            post.StatusId = (int)postApproval.StatusId;
            post.StatusChangedAt = DateTime.Now;
            post.StatusChangedBy = postApproval.ApprovedBy;
            _postRepository.Save();
        }

        public List<ThreadPostViewModel> GetAllApprovedPosts()
        {
            var posts = _postRepository.FindBy(x => x.Active && x.StatusId == (int)ThreadStatus.Approved && x.ParentId == null)
                .OrderByDescending(x => x.CreatedAt)
                .Select(x => new ThreadPostViewModel
            {
                Id = x.Id,
                CategoryId = x.CategoryId,
                Content = x.Content,
                CreatedBy = x.User.FullName,
                CreatedAt = x.CreatedAt,
                TotalLikes = x.PostLikes.Count,
                Tags = x.ThreadPostTags.Select(t => new TagViewModel { UserId = t.UserId, Name = t.User.FullName }),
                ChildThreads = x.ThreadPost1.OrderBy(p => p.CreatedAt)
                    .Select(p => new CommentViewModel
                {
                    Id = p.Id,
                    CategoryId = p.CategoryId,
                    Content = p.Content,
                    CreatedBy = p.User.FullName,
                    CreatedAt = p.CreatedAt,
                    Tags = p.ThreadPostTags.Select(v => new CommentTagViewModel { UserId = v.UserId, Name = v.User.FullName })
                })
            });

            return posts.ToList();
        }

        public List<ThreadPostViewModel> GetPostsForApproval()
        {
            var posts = _postRepository.FindBy(x => x.Active && x.StatusId == (int)ThreadStatus.Pending && x.ParentId == null)
                .OrderByDescending(x => x.CreatedAt)
                .Select(x => new ThreadPostViewModel
                {
                    Id = x.Id,
                    CategoryId = x.CategoryId,
                    Content = x.Content,
                    CreatedBy = x.User.FullName,
                    CreatedAt = x.CreatedAt,
                    Tags = x.ThreadPostTags.Select(t => new TagViewModel { UserId = t.UserId, Name = t.User.FullName })
                });

            return posts.ToList();
        }

        public int LikePost(LikePostModel post)
        {
            _postRepository.AddLikesToPost(post.UserId, post.PostId);
            return _postRepository.GetTotalLikes(post.PostId);
        }
    }
}