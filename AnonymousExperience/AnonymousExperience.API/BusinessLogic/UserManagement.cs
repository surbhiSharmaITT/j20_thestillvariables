﻿using System.Collections.Generic;
using AnonymousExperience.DAL.Repositories;
using AnonymousExperience.Model;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using AnonymousExperience.API.ViewModels;

namespace AnonymousExperience.API.BusinessLogic
{
    public class UserManagement : IUserManagement
    {
        private readonly IUserRepository _userRepository;

        public UserManagement(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<UserModel> GetAllUser()
        {
            var usersListModel = Mapper.Map<List<User>, List<UserModel>>(_userRepository.GetAll().ToList());
            return usersListModel;
        }

        public UserModel GetUserByUserId(int userId)
        {
            var userModel = Mapper.Map<User, UserModel>(_userRepository.FindBy(user => user.Id == userId).FirstOrDefault());
            return userModel;
        }

        public UserModel GetUserByUserName(string userName)
        {
            var result = Mapper.Map<User, UserModel>(_userRepository.FindBy(user => user.UserName == userName).FirstOrDefault());
            return result;
        }
    }
}
