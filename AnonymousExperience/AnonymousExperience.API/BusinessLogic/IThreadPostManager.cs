﻿using System.Collections.Generic;
using AnonymousExperience.API.ViewModels;

namespace AnonymousExperience.API.BusinessLogic
{
    public interface IThreadPostManager
    {
        void CreatePost(ThreadPostModel threadPost);
        void PostApproval(PostApprovalRequest postApproval);
        List<ThreadPostViewModel> GetAllApprovedPosts();
        List<ThreadPostViewModel> GetPostsForApproval();
        int LikePost(LikePostModel post);
    }
}