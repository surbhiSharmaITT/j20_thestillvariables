﻿using AutoMapper;
using AnonymousExperience.API.ViewModels;
using AnonymousExperience.Model;

namespace AnonymousExperience.API.Mappers
{
    public static class AutoMapperConfigurations
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<User, UserModel>().ReverseMap();
                cfg.CreateMap<ThreadPostModel, ThreadPost>().ReverseMap();
            });
        }
    }
}
