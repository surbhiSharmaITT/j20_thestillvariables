﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace AnonymousExperience.API.Helpers.ErrorHelper
{
    /// <summary>
    /// Api Exception
    /// </summary>
    [Serializable]
    [DataContract]
    public class ApiException : Exception, IApiExceptions
    {
        #region Public Serializable properties.
        /// <summary>
        /// Get/Set property for accessing Error Code
        /// </summary>
        [DataMember]
        public int ErrorCode { get; set; }
        /// <summary>
        /// Get/Set property for accessing Error Description
        /// </summary>
        [DataMember]
        public string ErrorDescription { get; set; }
        /// <summary>
        /// Get/Set property for accessing Http Status
        /// </summary>
        [DataMember]
        public HttpStatusCode HttpStatus { get; set; }

        string reasonPhrase = "ApiException";
        /// <summary>
        /// Get/Set property for accessing Reason Phrase
        /// </summary>
        [DataMember]
        public string ReasonPhrase
        {
            get { return this.reasonPhrase; }
            set { this.reasonPhrase = value; }
        }
        #endregion
    }
}