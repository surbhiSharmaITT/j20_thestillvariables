﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace AnonymousExperience.API.Helpers.ErrorHelper
{
    /// <summary>
    /// Api Business Exception
    /// </summary>
    [Serializable]
    [DataContract]
    public class ApiBusinessException : Exception, IApiExceptions
    {
        #region Public Serializable properties.
        /// <summary>
        /// Get/Set property for accessing Error Code
        /// </summary>
        [DataMember]
        public int ErrorCode { get; set; }
        /// <summary>
        /// Get/Set property for accessing Error Description
        /// </summary>
        [DataMember]
        public string ErrorDescription { get; set; }
        /// <summary>
        /// Get/Set property for accessing Http Status
        /// </summary>
        [DataMember]
        public HttpStatusCode HttpStatus { get; set; }

        string reasonPhrase = "ApiBusinessException";
        /// <summary>
        /// Get/Set property for accessing Reason Phrase
        /// </summary>
        [DataMember]
        public string ReasonPhrase
        {
            get { return this.reasonPhrase; }
            set { this.reasonPhrase = value; }
        }
        #endregion

        #region Public Constructor.
        /// <summary>
        /// Public constructor for Api Business Exception
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="errorDescription"></param>
        /// <param name="httpStatus"></param>
        public ApiBusinessException(int errorCode, string errorDescription, HttpStatusCode httpStatus)
        {
            ErrorCode = errorCode;
            ErrorDescription = errorDescription;
            HttpStatus = httpStatus;
        }
        #endregion

    }
}