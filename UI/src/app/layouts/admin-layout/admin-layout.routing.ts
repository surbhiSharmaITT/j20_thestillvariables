import { Routes } from '@angular/router';
import { DashboardComponent } from 'app/components/dashboard/dashboard.component';
import { AuthGuard } from 'app/guard/auth.guard';

export const AdminLayoutRoutes: Routes = [

  // Login

  // DashBoard
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'sports', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'technology', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'craft', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'spritual', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'party', component: DashboardComponent, canActivate: [AuthGuard] }
];
