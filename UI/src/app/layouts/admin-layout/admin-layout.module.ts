import { MustMatchDirective } from './../../helpers/must-match.directive';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedDataService } from 'app/service/common/sharedData.service';
import { DashboardComponent } from 'app/components/dashboard/dashboard.component'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ModalDialogService } from 'app/service/common/modal-dialog.service';
import { PostComponent } from 'app/components/dashboard/post/post.component';
import { FeaturedPostComponent } from 'app/components/dashboard/featured-post/featured-post.component';
import { AddPostComponent } from 'app/components/dashboard/add-post/add-post.component';
import { FileUploaderComponent } from 'app/components/dashboard/file-uploader/file-uploader.component';
import { LoginComponent } from 'app/components/common/login/login.component';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ModalModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      enableHtml: true
    }),
  ],
  declarations: [
    DashboardComponent,
    MustMatchDirective,
    PostComponent,
    FeaturedPostComponent,
    AddPostComponent,
    FileUploaderComponent,
    LoginComponent
  ],
  providers: [
    SharedDataService
  ],
})

export class AdminLayoutModule { }
