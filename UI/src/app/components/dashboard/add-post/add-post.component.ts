import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ModalDialogService } from 'app/service/common/modal-dialog.service';
import { ThreadPostModel } from 'app/model/post';
import { AuthService } from 'app/service/auth.service';
import { UserService } from 'app/service/user/user.service';
import { PostService } from 'app/service/post/post.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
  @ViewChild('popupModal', null) public popupModal: ModalDirective;
  postType: string;
  url: string;
  constructor(private modalService: ModalDialogService, private userService: UserService,
    private postService: PostService, private toastr: ToastrService) { }

  ngOnInit() {
    this.modalService.postWindow.subscribe((visibility: boolean) => {
      if (visibility) {
        this.postType = this.userService.isUserLoggedIn() ? 'withlogin' : 'annonymous';
        this.show();
      } else {
        this.hide();
      }
    });
  }
  show() {
    setTimeout(() => {
      this.popupModal.show();
    });

  }
  hide() {
    this.popupModal.hide();
  }

  post(input) {
    if (!input.value && input.value.trim() === '') {
      alert('Please add some thoughts...');
    } else {
      const post = new ThreadPostModel();
      post.content = input.value.trim();
      const userId = this.postType === 'annonymous' ? null : this.userService.getUser().id;
      post.createdBy = parseInt(userId, 10);
      this.postService.createPostOrReply(post).subscribe(res => {
        this.toastr.success('Your post has been sent for review successfully.');
      });
      this.clearInputs(input);
    }
  }
  ImageUrl(value) {
    this.url = value;
  }
  clearInputs(input) {
    this.url = null;
    input.value = '';
    this.popupModal.hide();
  }

}
