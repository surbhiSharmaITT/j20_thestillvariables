import { Component, OnInit } from '@angular/core';
import { ThreadPostViewModel, ThreadPostModel, PostApprovalRequest, ThreadStatus } from 'app/model/post';
import { AuthService } from 'app/service/auth.service';
import { ModalDialogService } from 'app/service/common/modal-dialog.service';
import { UserService } from 'app/service/user/user.service';
import { PostService } from 'app/service/post/post.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  posts: ThreadPostViewModel[];
  isAdminUser: boolean;
  constructor(private userService: UserService, private modalService: ModalDialogService,
    private postService: PostService, private toastr: ToastrService) { }

  ngOnInit() {
    this.isAdminUser = this.userService.isAdminUser();
    if (this.isAdminUser) {
      this.postService.getPostsToApprove().subscribe((data: ThreadPostViewModel[]) => {
        this.posts = data;
      });
    } else {
      this.postService.getApprovedPosts().subscribe((data: ThreadPostViewModel[]) => {
        this.posts = data;
      })
    }
  }

  getTime(value) {
    const ms = new Date().getTime() - value;
    const hrs = Math.floor((ms / 1000 / 60 / 60) << 0);
    const min = Math.floor((ms / 1000 / 60) << 0);
    const seconds = Math.floor((ms / 1000) % 60);
    if (hrs > 0) {
      return hrs + ' hours ago';
    } else if (min > 0) {
      return min + ' min ago';
    } else {
      return 'Just now';
    }
  }

  comment(commentInput, post: ThreadPostViewModel) {
    if (this.userService.isUserLoggedIn()) {
      if (!commentInput.value && commentInput.value.trim() === '') {
        alert('Please add some thoughts...');
      } else {
        const comment = new ThreadPostViewModel();
        comment.content = commentInput.value.trim();
        comment.createdBy = this.userService.getUser().id;
        this.postService.createPostOrReply(post).subscribe(res => {
          comment.createdBy = this.userService.getUser().fullName;
          post.childThreads.push(comment);
          commentInput.value = '';
        });
      }
    } else {
      this.modalService.openLoginWindow(true);
    }
  }

  reviewPost(postId, statusId) {
    const approvalRequest = new PostApprovalRequest();
    approvalRequest.threadPostId = postId;
    approvalRequest.statusId = statusId;
    approvalRequest.approvedBy = parseInt(this.userService.getUser().id, 10);
    this.postService.approvePost(approvalRequest).subscribe((response) => {
      if (statusId === ThreadStatus.approved) {
        this.toastr.success('Post approved successfully...');
      } else {
        this.toastr.success('Post declined succesfully..')
      }
     this.posts = this.posts.filter(post => post.id !== postId);
    });
  }
}
