import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/service/user/user.service';
import { Token } from 'app/model/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  isAdminUser: boolean;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.userDetails.subscribe((data: Token) => {
      this.isAdminUser = this.userService.isAdminUser();
    });
  }

}
