import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'app/service/auth.service';
import { LoginUser, Token } from 'app/model/user';
import { ModalDialogService } from 'app/service/common/modal-dialog.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'app/service/user/user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginDailog', null) public loginDialog: ModalDirective;
  token: any;
  user: LoginUser;

  constructor(private _modalService: ModalDialogService, private _authService: AuthService,
    private toastr: ToastrService, private userService: UserService) { }

  ngOnInit() {
    this.user = new LoginUser();
    this._modalService.loginWindow.subscribe((value) => {
      if (value) {
        this.show();
      } else {
        this.hide();
      }
    });
  }

  show() {
    setTimeout(() => {
      this.loginDialog.show();
    });

  }
  hide() {
    this.loginDialog.hide();
  }
  onLogin(formValues: any) {
    if (formValues.username !== '' && formValues.password !== '') {
      const body = new URLSearchParams();
      body.set('username', formValues.username);
      body.set('password', formValues.password);
      body.set('grant_type', 'password');

      this._authService.getToken(body.toString())
        .subscribe((token: Token) => {
          if (token) {
            this.token = token;
            this._authService.accessToken = this.token.access_token;
            sessionStorage.setItem('userToken', this.token.access_token);
            sessionStorage.setItem('user', JSON.stringify(token));
            this.userService.setUserDetails(token);
            this.hide();
            this.toastr.success('loggged in succesfully...');
          }

        });
    }
  }

}

