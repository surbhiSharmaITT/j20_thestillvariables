import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'app/service/common/loading.service';


@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  loadingStatus: boolean;

  constructor(private _loadingService: LoadingService) { }

  ngOnInit() {
    this._loadingService.loadingSpinnerIsActive.subscribe(toggleState => this.loadingStatus = toggleState);

    this._loadingService.activeDownloadIds.subscribe(data => {
      if (data && data.length > 0) {
       this._loadingService.updateLoadingSpinner(true);
      } else {
        this._loadingService.updateLoadingSpinner(false);
      }
    });
  }

}
