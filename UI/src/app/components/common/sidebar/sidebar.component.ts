import { Component, OnInit } from '@angular/core';
import { ModalDialogService } from 'app/service/common/modal-dialog.service';


export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    adminAccess: boolean
}

export const ROUTES: RouteInfo[] = [
    { path: '/party', title: 'Party', icon: 'nc-briefcase-24', class: '', adminAccess: false },
    { path: '/sports', title: 'Sports', icon: 'nc-pin-3', class: '', adminAccess: false },
    { path: '/technology', title: 'Technology', icon: 'nc-ambulance', class: '', adminAccess: false },
    { path: '/craft', title: 'Craft', icon: 'nc-chart-bar-32', class: '', adminAccess: false },
    { path: '/spritual', title: 'Spritual', icon: 'nc-single-02', class: '', adminAccess: true },
    { path: '/dashboard', title: 'All Feed', icon: 'nc-bank', class: '', adminAccess: false },
];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];

    constructor(private modalService: ModalDialogService) {

    }
    ngOnInit() {
        const userDetails = sessionStorage.getItem('user');
        if (userDetails && JSON.parse(userDetails).roleName === 'admin') {
            this.menuItems = ROUTES.filter(menuItem => menuItem);
        } else {
            this.menuItems = ROUTES.filter(menuItem => !menuItem.adminAccess);
        }
    }

    createNewPost() {
        this.modalService.openPostWindow(true);
    }
}
