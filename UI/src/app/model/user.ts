import { Role } from './role';

export class User {
  id: number;
  firstName: string;
  userName: string;
  email: string;
  phoneNumber: number;
  token: string;

  constructor() {
    this.id = 0;
  }

}

export class LoginUser {
  username: string;
  password: string;
}

export class Token {
  access_token: string;
  token_type: string;
  expires_in: number;
  fullName: string;
  email: string;
  role: string;
  id: string;
}
