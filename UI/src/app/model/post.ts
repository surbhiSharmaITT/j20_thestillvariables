export class ThreadPostModel {
    id: number;
    content: string;
    categoryId: number;
    createdAt: Date;
    createdBy: number;
    parentId: number;
    tags: ThreadPostTagModel[];
    constructor() {
        this.tags = [];
    }
}

export class ThreadPostTagModel {
    id: number;
    userId: number;
    threadPostId: number;
}

export class ThreadPostViewModel {
    id: number;
    content: string;
    categoryId: number;
    createdBy: string;
    tags: ThreadPostTagModel[];
    childThreads: ThreadPostViewModel[];
    username: string;
    imageURL: string;
    createdOn: Date;
    commentsCount: number;
    likesCount: number;
    commentVisibility: boolean;
}

export class TagViewModel {
    userId: number;
    name?: any;
}

export const ThreadStatus = {
    approved : 2,
    rejected: 3,
    pending: 1
}

export class PostApprovalRequest {
    threadPostId: number;
    statusId?: any;
    approvedBy: number;
}
