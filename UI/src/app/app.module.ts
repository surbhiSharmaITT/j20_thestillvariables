import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AppService } from './service/app.service';
import { SharedDataService } from './service/common/sharedData.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderComponent } from './components/common/loader/loader.component';
import { NavbarModule } from './components/common/navbar/navbar.module';
import { FooterModule } from './components/common/footer/footer.module';
import { SidebarModule } from './components/common/sidebar/sidebar.module';
import { LoadingService } from './service/common/loading.service';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HeaderComponent } from './components/common/header/header.component';
import { AuthService } from './service/auth.service';
import { AuthGuard } from './guard/auth.guard';
import { ModalDialogService } from './service/common/modal-dialog.service';
import { UserService } from './service/user/user.service';

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoaderComponent,
    HeaderComponent
  ],
  imports: [
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes, {
      useHash: false
    }),
    SidebarModule,
    NavbarModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      enableHtml: true
    }),
    FooterModule,
    HttpClientModule,
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    AppService,
    LoadingService,
    SharedDataService,
    AuthService,
    ModalDialogService,
    UserService,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
