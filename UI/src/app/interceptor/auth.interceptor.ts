import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';
import { LoadingService } from 'app/service/common/loading.service';
import { AuthService } from 'app/service/auth.service';
// import { AuthService } from '../services/auth.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private injector: Injector,
    private _authService: AuthService
  ) { }


  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // injecting the service dependencies using injector
    const _loadingService = this.injector.get(LoadingService);


    _loadingService.addElementToActiveDownloadIds(req.url);
    if (!req.url.includes('/authenticate')) {
      req = this.addAuthenticationToken(req);
    }
    if (req.url.includes('/import')) {
      req.clone({
        headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
          'Access-Control-Allow-Origin': '*',
          'Authorization': 'Bearer' + sessionStorage.getItem('userToken')
        }),
        responseType: 'text'
      });
    }
    if (!req.headers.has('Content-Type')) {
      req.clone({
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        })
      });
    }

    return next.handle(req).pipe(
      map(response => {
        return response
      }),
      catchError((error: HttpErrorResponse) => {
        if (error && error.status === 401) {
          this._authService.logout(error.error);
        } else {
          this._authService.ShowError();
        }
        return of(null);
      }),
      finalize(() => _loadingService.removeElementToActiveDownloadIds(req.url))
    );
  }

  private addAuthenticationToken(request: HttpRequest<any>, newToken?: string): HttpRequest<any> {
    // Here we could first retrieve the token from where we store it.

    const token = sessionStorage.getItem('userToken');
    if (!token) {
      return request;
    }

    return request.clone({
      headers: request.headers.set('Authorization', 'bearer ' + token)
    });
  }

}
