import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
// import { LoadingService } from './loading.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Authorization': 'Bearer' + sessionStorage.getItem('userToken')
  }),
};

const httpOptionsWithParams = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }),
  params: {}
};

@Injectable()
export class AppService {

  constructor(private http: HttpClient,
    // private loadingService: LoadingService
  ) { }

  /**
   * Handle get call
   * @param url - url
   */
  getAction(url: string) {
    return this.http.get(url, httpOptions);
  }

  /**
   * Handle get call with params
   * @param url - url
   */
  getActionWithParam(url: string, httpParams: HttpParams) {
    return this.http.get(url, httpOptions);
  }


  /** POST: add a new hero to the server */
  postAction(url: string, body: any) {

    return this.http.post(url, body, httpOptions);
  }

  postImportAction(url: string, body: any) {
    return this.http.post(url, body);
  }

  /** PUT: update the hero on the server */
  putAction(url: string, body: any) {
    return this.http.put(url, body, httpOptions);
  }

  /** DELETE: delete the hero from the server */
  deleteAction(url: string) {
    return this.http.delete(url, httpOptions);
  }

  /** POST: add a new hero to the server */
  fileUploadProgress(url: string, body: any) {
    return this.http.post(url, body);
  }
}
