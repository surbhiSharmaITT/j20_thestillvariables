import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { catchError, map, finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LoadingService } from './common/loading.service';
import { User, Token } from 'app/model/user';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Access-Control-Allow-Origin': environment.endpoint
  })
};

const actionUrls = {
  token: environment.endpoint + 'token'
};

@Injectable()
export class AuthService {
  accessToken: string;

  constructor(private http: HttpClient, private toastr: ToastrService, private _router: Router, private _loadingService: LoadingService) { }

  getToken(user) {
    // needs to be chnaged as post
    return this.http.post(actionUrls.token, user, httpOptions).pipe(
      map(response => response),
      catchError(this.handleError()),
      finalize(() => { this._loadingService.removeAllElementFromActiveDownloadList(); })
    );
  }

  private handleError<T>(operation = 'Operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.status >= 400 && error.status <= 451) {
        if (error.error !== undefined && error.error !== null) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error(`${operation} failed: ${error.error.error_description !== undefined ? error.error.error_description : error.error.message}`);
        } else {
          this.toastr.error(`${operation} failed: ${error.statusText}`);
        }
      }
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  logout(error?: any) {
    sessionStorage.clear();
    this.toastr.success('Logged out successfully.')

    // if (error && error.message) {
    //   this.toastr.error(error.message);
    // } else {
    //   this.toastr.error('Session timed out, please login again.');
    // }
    // this._router.navigate(['/login']);
  }


  ShowError() {
    this.toastr.error('Something went wrong, please try again.');
  }
}
