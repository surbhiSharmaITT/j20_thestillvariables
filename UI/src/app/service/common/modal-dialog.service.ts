import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalDialogService {
  private addPostSource = new BehaviorSubject(false);
  postWindow = this.addPostSource.asObservable();

  private loginSource = new BehaviorSubject(false);
  loginWindow = this.loginSource.asObservable();
  openPostWindow(value: boolean) {
    this.addPostSource.next(value)
  }

  openLoginWindow(value) {
    this.loginSource.next(true);
  }
  constructor() { }
}
