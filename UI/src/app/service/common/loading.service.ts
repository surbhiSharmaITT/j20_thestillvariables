import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class LoadingService {
  // Observable that controls (and indicates to subsribers) whether the full-page HP loading spinner is active
  private _loadingSpinnerIsActive = new BehaviorSubject<boolean>(false);
  loadingSpinnerIsActive = this._loadingSpinnerIsActive.asObservable();

  // Observable that maintains a list of current downloads-in-progress
  private _activeDownloadIds = new BehaviorSubject<string[]>([]);
  activeDownloadIds = this._activeDownloadIds.asObservable();

  loadingSpinnerIsLocked = false;

  constructor() { }

  addElementToActiveDownloadIds(item) {
    this.activeDownloadIds.pipe(take(1)).subscribe(val => {
      const newArr = [...val, item];
      this._activeDownloadIds.next(newArr);
    });
  }
  removeElementToActiveDownloadIds(item) {
    this.activeDownloadIds.pipe(take(1)).subscribe(val => {
      const arr = val;
      arr.splice(arr.indexOf(item), 1);
      this._activeDownloadIds.next(arr);
    });
  }
  /**
   * Sends the given value to subscribers to the 'hpLoadingSpinnerIsActive' observable
   * @param value : TRUE to turn 'ON' the full-page HP loading spinner, or FALSE to turn it 'OFF'
   */
  updateLoadingSpinner(value: boolean): this {
    // Allow the loading spinner to be 'locked' in its current state, preventing premature cancellation
    if (!this.loadingSpinnerIsLocked) {
      this._loadingSpinnerIsActive.next(value);
      return this; // Allow method-chaining
    }
  }

  /**
   * remove all the active downloads from list if user get logged out.
   */
  removeAllElementFromActiveDownloadList() {
    this._activeDownloadIds.next([]);
  }
  /**
   * Lock the loading spinner in its current state, i.e. true/ON or false/OFF
   */
  lockLoadingSpinner(): this {
    this.loadingSpinnerIsLocked = true;
    return this; // Allow method-chaining
  }

  /**
   * Release the loading spinner from its locked state
   */
  unlockLoadingSpinner(): this {
    this.loadingSpinnerIsLocked = false;
    return this; // Allow method-chaining
  }
}
