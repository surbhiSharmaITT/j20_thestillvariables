import { AppService } from './../app.service';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

const actionUrls = {
  getApprovedPosts: environment.endpoint + 'Post/GetApproved',
  createPostOrReply: environment.endpoint + 'Post/Create',
  approvePost: environment.endpoint + 'Post/Approve',
  getPostsToApprove: environment.endpoint + 'Post/Approve',
  likePost: environment.endpoint + 'Post/Like',
};


@Injectable({
  providedIn: 'root'
})

export class PostService extends AppService {

  getApprovedPosts() {
    return this.getAction(actionUrls.getApprovedPosts);
  }

  getPostsToApprove() {
    return this.getAction(actionUrls.getPostsToApprove);
  }

  createPostOrReply(data) {
    return this.postAction(actionUrls.createPostOrReply, data);
  }

  approvePost(data) {
    return this.postAction(actionUrls.approvePost, data);
  }

  likePost(data) {
    return this.postAction(actionUrls.likePost, data);
  }

}

