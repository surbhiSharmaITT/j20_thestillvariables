import { AppService } from './../app.service';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Token } from 'app/model/user';

const actionUrls = {
  getAllUser: environment.apiEndPoint + 'User/GetAllUser',
  getUserById: environment.apiEndPoint + 'User/GetUserById?userId=',
  getUserByUserName: environment.apiEndPoint + 'User/getUserByUserName?userName=',
  register: environment.apiEndPoint + 'User/Register',
};


@Injectable({
  providedIn: 'root'
})

export class UserService extends AppService {

  private userSource = new BehaviorSubject({});
  private userData: Token;
  userDetails = this.userSource.asObservable();

  setUserDetails(userDetails: Token) {
    this.userData = userDetails;
    this.userSource.next(userDetails);
  }

  getUser() {
    let user = new Token();
    if (this.isUserLoggedIn()) {
      user = JSON.parse(sessionStorage.getItem('user'));
    }
    return user;
  }


  isUserLoggedIn() {
    const user = sessionStorage.getItem('user');
    if (user && (Object.keys(JSON.parse(user)).length > 0)) {
      return true;
    }
    return false;
  }

  getAllUser() {
    return this.getAction(actionUrls.getAllUser);
  }

  getUserById(userId) {
    return this.getAction(actionUrls.getUserById + userId);
  }

  getUserByUserName(userName) {
    return this.getAction(actionUrls.getUserByUserName + userName);
  }

  register(data) {
    return this.postAction(actionUrls.register, data);
  }

  isAdminUser() {
    const user = this.getUser();
    if (user && user.role === 'Admin') {
      return true;
    }
    return false;
  }
}
